import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../shared/user.interface';
import { NgForm } from '@angular/forms';
import { AuthStoreService } from '../auth-store.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  @ViewChild('f')
  public registrationForm: NgForm;
  public user: User = { email: null, password: null };
  constructor(private authStoreService: AuthStoreService) {}

  ngOnInit() {}

  public onRegister() {
    if (this.registrationForm.valid) {
      this.authStoreService.signUp(this.user);
    }
  }
}
