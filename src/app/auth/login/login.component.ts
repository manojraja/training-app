import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../shared/user.interface';
import { AuthStoreService } from '../auth-store.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('f')
  public loginForm: NgForm;
  public user: User = { email: null, password: null };
  constructor(private authStoreService: AuthStoreService) {}

  ngOnInit() {
    this.authStoreService
      .getUserLoggedInFlag()
      .pipe(take(1))
      .subscribe(data => {
        if (data) {
          this.authStoreService.logout();
          alert('You are logged out');
        }
      });
  }

  public onLogin() {
    if (this.loginForm.valid) {
      this.authStoreService.signIn(this.user);
    }
  }
}
