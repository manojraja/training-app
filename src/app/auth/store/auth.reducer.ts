import { IAuthState } from './auth-state.interface';
import { AuthActions, AuthActionTypes } from './auth.actions';

export const initialState: IAuthState = {
  authToken: null,
  isUserLoggedIn: false
};

export function authReducer(
  state = initialState,
  action: AuthActions
): IAuthState {
  switch (action.type) {
    case AuthActionTypes.SET_AUTH_TOKEN:
      return { ...state, authToken: action.payload };
    case AuthActionTypes.SET_USER_LOGGED_IN_FLAG:
      return { ...state, isUserLoggedIn: action.payload };
    case AuthActionTypes.SIGNOUT:
      return { ...state, authToken: null, isUserLoggedIn: false };
    default:
      return state;
  }
}
