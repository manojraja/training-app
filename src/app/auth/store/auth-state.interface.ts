export interface IAuthState {
  authToken: string;
  isUserLoggedIn: boolean;
}
