import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import {
  map,
  filter,
  switchMap,
  mergeMap,
  tap
} from 'rxjs/operators';
import * as firebase from 'firebase';
import {
  AuthActionTypes,
  SignUp,
  SetAuthToken,
  SetUserLoggedInFlag
} from './auth.actions';
import { from } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {
  @Effect({ dispatch: false })
  signUp = this.actions$.ofType(AuthActionTypes.SIGNUP).pipe(
    filter((action: SignUp) => {
      return typeof action !== 'undefined';
    }),
    switchMap((action: SignUp) => {
      return from(
        firebase
          .auth()
          .createUserWithEmailAndPassword(
            action.user.email,
            action.user.password
          )
      );
    }),
    map(() => alert('Registration successful, please login'))
  );

  @Effect()
  signIn = this.actions$.ofType(AuthActionTypes.SIGNIN).pipe(
    filter((action: SignUp) => Boolean(action.user.email)),
    switchMap((action: SignUp) =>
      from(
        firebase
          .auth()
          .signInWithEmailAndPassword(action.user.email, action.user.password)
      )
    ),
    switchMap(() => firebase.auth().currentUser.getIdToken()),
    mergeMap(token => [new SetAuthToken(token), new SetUserLoggedInFlag(true)]),
    tap(() => this.router.navigate(['home']))
  );

  constructor(private actions$: Actions, private router: Router) {}
}
