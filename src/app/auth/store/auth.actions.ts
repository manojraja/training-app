import { Action } from '@ngrx/store';
import { User } from '../../shared/user.interface';

export enum AuthActionTypes {
  SIGNUP = '[Auth] Sign up',
  SET_AUTH_TOKEN = '[Auth] Set Auth Token',
  SET_USER_LOGGED_IN_FLAG = '[Auth] Set User Logged In',
  SIGNIN = '[Auth] Sign in',
  SIGNOUT = '[Auth] Sign out'
}

export class SignUp implements Action {
  readonly type = AuthActionTypes.SIGNUP;
  constructor(public user: User) {}
}

export class SignIn implements Action {
  readonly type = AuthActionTypes.SIGNIN;
  constructor(public user: User) {}
}

export class SetAuthToken implements Action {
  readonly type = AuthActionTypes.SET_AUTH_TOKEN;
  constructor(public payload: string) {}
}

export class SetUserLoggedInFlag implements Action {
  readonly type = AuthActionTypes.SET_USER_LOGGED_IN_FLAG;
  constructor(public payload: boolean) {}
}

export class SignOut implements Action {
  readonly type = AuthActionTypes.SIGNOUT;
}

export type AuthActions = SetAuthToken | SetUserLoggedInFlag | SignUp | SignIn | SignOut;
