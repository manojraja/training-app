import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../store/app-state.interface';
import {
  SetAuthToken,
  SetUserLoggedInFlag,
  SignUp,
  SignIn,
  SignOut
} from './store/auth.actions';
import { Observable } from 'rxjs';
import { User } from '../shared/user.interface';

@Injectable()
export class AuthStoreService {
  constructor(private store: Store<IAppState>) {}

  public signUp(user: User) {
    if (Boolean(user)) {
      this.store.dispatch(new SignUp(user));
    }
  }

  public signIn(user: User) {
    if (Boolean(user)) {
      this.store.dispatch(new SignIn(user));
    }
  }

  public login(token: string) {
    this.setAuthToken(token);
    this.setUserLoggedInFlag(true);
  }

  public setAuthToken(token: string) {
    this.store.dispatch(new SetAuthToken(token));
  }

  public getAuthToken(): Observable<string> {
    return this.store.select('auth', 'authToken');
  }

  public setUserLoggedInFlag(flag: boolean) {
    this.store.dispatch(new SetUserLoggedInFlag(flag));
  }

  public getUserLoggedInFlag(): Observable<boolean> {
    return this.store.select('auth', 'isUserLoggedIn');
  }

  public logout() {
    this.store.dispatch(new SignOut());
  }
}
