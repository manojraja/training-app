import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpParams
} from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Injectable, OnDestroy } from '@angular/core';
import { AuthStoreService } from '../auth/auth-store.service';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor, OnDestroy {
  private unsubscribe = new Subject();
  private token: string = null;

  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const queryParams = new HttpParams().set('auth', this.token);
    const modifiedRequest: HttpRequest<any> = request.clone({
      params: queryParams
    });
    return next.handle(modifiedRequest);
  }

  constructor(private authStoreService: AuthStoreService) {
    this.authStoreService
      .getAuthToken()
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(data => (this.token = data));
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
