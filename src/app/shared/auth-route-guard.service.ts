import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  CanLoad
} from '@angular/router';
import { Observable } from 'rxjs';
import { Route } from '@angular/compiler/src/core';
import { AuthStoreService } from '../auth/auth-store.service';
import { take } from 'rxjs/operators';

@Injectable()
export class AuthRouteGuardService implements CanActivate, CanLoad {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean {
    return this.authMethod();
  }

  constructor(
    private authStoreService: AuthStoreService,
    private router: Router
  ) {}

  canLoad(route: Route): Observable<boolean> | boolean {
    return this.authMethod();
  }

  private authMethod(): Observable<boolean> | boolean {
    let authFlag = false;
    this.authStoreService
      .getUserLoggedInFlag()
      .pipe(take(1))
      .subscribe(data => (authFlag = data));

    if (authFlag) {
      return true;
    } else {
      alert('Please login bofore accessing the application');
      this.router.navigate(['login']);
    }
  }
}
