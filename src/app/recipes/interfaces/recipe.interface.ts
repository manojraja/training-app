import { Ingredient } from '../../shared/ingredient.model';

export interface IIndexedRecipe {
  [id: number]: IRecipe;
}

export interface IRecipe {
  id: number;
  name: string;
  description: string;
  imagePath: string;
  ingredients: Array<Ingredient>;
}
