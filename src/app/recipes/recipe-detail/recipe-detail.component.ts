import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IRecipe } from '../interfaces/recipe.interface';
import { Observable, Subject } from 'rxjs';
import { RecipeStoreService } from '../services/recipe-store.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit, OnDestroy {
  public id: number;
  public recipe$: Observable<IRecipe>;
  public unsubscribe = new Subject();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private recipeStoreService: RecipeStoreService
  ) {
    this.route.params.pipe(take(1)).subscribe(params => {
      this.id = Number(params['id']);
    });
  }

  ngOnInit() {
    this.recipe$ = this.recipeStoreService.getSelectedRecipe();
  }

  public addToShoppingList() {
    this.recipeStoreService.addIngredientsToShoppingList();
  }

  public goToEditRecipe() {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  public deleteRecipe() {
    this.recipeStoreService.deleteRecipe(this.id);
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
