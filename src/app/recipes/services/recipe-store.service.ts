import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IRecipe } from '../interfaces/recipe.interface';
import { Store } from '@ngrx/store';
import { IRecipeAppState } from '../store/recipe-state.interface';
import {
  GetRecipes,
  SetSelectedRecipe,
  DeleteRecipe,
  UpdateSelectedRecipe,
  AddIngredientToSelectedRecipe,
  DeleteIngredientFromSelectedRecipe,
  UpdateRecipe,
  AddRecipe
} from '../store/recipe.actions';
import { take } from 'rxjs/operators';
import { ShoppingListStoreService } from '../../shopping-list/shopping-list-store.service';

@Injectable()
export class RecipeStoreService {
  public getRecipes(): Observable<Array<IRecipe>> {
    this.store.dispatch(new GetRecipes());
    return this.store.select('recipes', 'recipes');
  }

 public setSelectedRecipe(recipe: IRecipe) {
    this.store.dispatch(new SetSelectedRecipe(recipe));
  }

  public getSelectedRecipe(): Observable<IRecipe> {
    return this.store.select('recipes', 'selectedRecipe');
  }

  public addIngredientsToShoppingList() {
    this.getSelectedRecipe()
      .pipe(take(1))
      .subscribe(data =>
        data.ingredients.map(ingredient =>
          this.shoppingListStoreService.addIngredient(ingredient, true)
        )
      );
  }

  public deleteRecipe(id: number) {
    return this.store.dispatch(new DeleteRecipe(id));
  }

  public createNewRecipe() {
    const recipe: IRecipe = {
      description: null,
      id: null,
      imagePath: null,
      ingredients: [],
      name: null
    };
    this.store.dispatch(new SetSelectedRecipe(recipe));
  }

  public updateRecipe() {
    this.store.dispatch(new UpdateRecipe());
  }

  public addRecipe() {
    this.store.dispatch(new AddRecipe());
  }

  public updateSelectedRecipe(key, value: any) {
    this.store.dispatch(new UpdateSelectedRecipe(key, value));
  }

  public addNewIngredientToSelectedRecipe() {
    this.store.dispatch(new AddIngredientToSelectedRecipe());
  }

  public deleteIngredientFromSelectedRecipe(index: number) {
    this.store.dispatch(new DeleteIngredientFromSelectedRecipe(index));
  }

  constructor(
    private store: Store<IRecipeAppState>,
    private shoppingListStoreService: ShoppingListStoreService
  ) {}
}
