import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, of as observableOf } from 'rxjs';
import { IIndexedRecipe, IRecipe } from '../interfaces/recipe.interface';

@Injectable()
export class RecipeApiService {
  public baseUrl = 'https://training-app-62f03.firebaseio.com';

  constructor(private http: HttpClient) {}

  public getRecipes(): Observable<Array<IRecipe>> {
    const apiUrl = `${this.baseUrl}/recipes.json`;

    return this.http
      .get(apiUrl)
      .pipe(map((response: IIndexedRecipe) => this.constructRecipes(response)));
  }

  public createRecipe(id: number, recipe: IRecipe): Observable<IIndexedRecipe> {
    const apiUrl = `${this.baseUrl}/recipes.json`;
    const iIndexedRecipe: IIndexedRecipe = {
      [id]: recipe
    };

    const response = this.http.patch(apiUrl, iIndexedRecipe).pipe(
      catchError(error => {
        return observableOf(error);
      })
    );

    return response;
  }

  public updateRecipe(id: number, recipe: IRecipe): Observable<IIndexedRecipe> {
    const apiUrl = `${this.baseUrl}/recipes/${id}.json`;
    return this.http.put(apiUrl, recipe).pipe(
      map(data => data),
      catchError(error => observableOf(error))
    );
  }

  public deleteRecipe(id: number) {
    const apiUrl = `${this.baseUrl}/recipes/${id}.json`;
    return this.http.delete(apiUrl).pipe(
      map(data => data),
      catchError(error => {
        return observableOf(error);
      })
    );
  }

  public constructRecipes(indexedRecipes: IIndexedRecipe) {
    const recipeList = [];
    if (Boolean(indexedRecipes)) {
      Object.keys(indexedRecipes).map(data =>
        recipeList.push(indexedRecipes[data])
      );
    }
    return recipeList;
  }
}
