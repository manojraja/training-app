import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IRecipe } from '../interfaces/recipe.interface';
import { Observable, Subject } from 'rxjs';
import { NgForm } from '@angular/forms';
import { RecipeStoreService } from '../services/recipe-store.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit, OnDestroy {
  @ViewChild('f')
  recipeEditForm: NgForm;
  public id: number;
  public isEditMode = false;
  public recipe$: Observable<IRecipe>;
  public unsubscribe = new Subject();


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private recipeStoreService: RecipeStoreService
  ) {
    this.route.params.pipe(take(1)).subscribe(params => {
      this.id = Number(params['id']);
      this.isEditMode = typeof params['id'] === 'undefined' ? false : true;
    });
  }

  ngOnInit() {
    this.recipe$ = this.recipeStoreService.getSelectedRecipe();
  }

  public addIngredient() {
    this.recipeStoreService.addNewIngredientToSelectedRecipe();
  }

  public deleteIngredient(index: number) {
    this.recipeStoreService.deleteIngredientFromSelectedRecipe(index);
  }

  public updateSelectedRecipe(key, value) {
    this.recipeStoreService.updateSelectedRecipe(key, value);
  }

  public onSave() {
    if (this.recipeEditForm.valid) {
      if (this.isEditMode) {
        this.recipeStoreService.updateRecipe();
      } else {
        this.updateSelectedRecipe('id', this.generateId());
        this.recipeStoreService.addRecipe();
      }
    } else {
      Object.keys(this.recipeEditForm.controls)
        .filter(key => !this.recipeEditForm.controls[key].touched)
        .map(key => this.recipeEditForm.controls[key].markAsTouched());
    }
  }

  public onCancel() {
    this.isEditMode
      ? this.router.navigate(['recipes', this.id])
      : this.router.navigate(['recipes']);
  }

  public generateId(): number {
    return Math.floor(Math.random() * 100);
  }

  public trackByFn(index: number) {
    return index;
  }

  public showError(name: string) {
    return Boolean(this.recipeEditForm) &&
      Boolean(this.recipeEditForm.controls) &&
      Boolean(this.recipeEditForm.controls[name])
      ? this.recipeEditForm.controls[name].invalid &&
          this.recipeEditForm.controls[name].touched
      : false;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
