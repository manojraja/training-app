import { RecipeActions, RecipeActionsTypes } from './recipe.actions';
import { IRecipeState } from './recipe-state.interface';
import { assocPath } from 'ramda';
import { Ingredient } from '../../shared/ingredient.model';

export const initialState: IRecipeState = {
  recipes: [],
  selectedRecipe: {
    description: null,
    id: null,
    imagePath: null,
    ingredients: [],
    name: null
  }
};

export function recipeReducer(state = initialState, action: RecipeActions) {
  switch (action.type) {
    case RecipeActionsTypes.SET_RECIPES:
      return {
        ...state,
        recipes: [...action.recipes]
      };
    case RecipeActionsTypes.SET_SELECTED_RECIPE:
      return {
        ...state,
        selectedRecipe: action.recipe
      };
    case RecipeActionsTypes.UPDATE_SELECTED_RECIPE:
      return Array.isArray(action.key)
        ? assocPath(['selectedRecipe', ...action.key], action.value, state)
        : assocPath(['selectedRecipe', action.key], action.value, state);
    case RecipeActionsTypes.ADD_INGREDIENT_TO_SELECTED_RECIPE: {
      const newIngredient: Ingredient = { name: null, quantity: null };
      assocPath(['selectedRecipe', 'ingredients'], [newIngredient], state);
      return typeof state.selectedRecipe.ingredients === 'undefined'
        ? assocPath(['selectedRecipe', 'ingredients'], [newIngredient], state)
        : assocPath(
            ['selectedRecipe', 'ingredients'],
            [...state.selectedRecipe.ingredients, newIngredient],
            state
          );
    }
    case RecipeActionsTypes.DELETE_INGREDIENT_FROM_SELECTED_RECIPE:
      return assocPath(
        ['selectedRecipe', 'ingredients'],
        [...state.selectedRecipe.ingredients].splice(action.index, 1),
        state
      );
    default:
      return state;
  }
}
