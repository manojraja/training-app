import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { RecipeApiService } from '../services/recipe-api.service';
import {
  RecipeActionsTypes,
  SetRecipes,
  DeleteRecipe,
  GetRecipes,
  RouteToRecipes
} from './recipe.actions';
import {
  map,
  switchMap,
  mergeMap,
  mergeMapTo,
  flatMap,
  filter,
  take
} from 'rxjs/operators';
import { Router } from '@angular/router';
import { RecipeStoreService } from '../services/recipe-store.service';

@Injectable()
export class RecipeEffects {
  @Effect()
  getRecipes$ = this.actions$.ofType(RecipeActionsTypes.GET_RECIPES).pipe(
    flatMap(() => this.recipeApiService.getRecipes()),
    map(recipeList => new SetRecipes(recipeList))
  );

  @Effect()
  deleteRecipe$ = this.actions$.ofType(RecipeActionsTypes.DELETE_RECIPE).pipe(
    switchMap((action: DeleteRecipe) =>
      this.recipeApiService.deleteRecipe(action.index)
    ),
    mergeMapTo([new GetRecipes(), new RouteToRecipes()])
  );

  @Effect({ dispatch: false })
  routeToRecipes$ = this.actions$.ofType(RecipeActionsTypes.GO_TO_RECIPES).pipe(
    map((action: RouteToRecipes) => {
      const route =
        typeof action.id === 'undefined' ? ['recipes'] : ['recipes', action.id];
      this.router.navigate(route);
    })
  );

  @Effect()
  updateRecipe$ = this.actions$.ofType(RecipeActionsTypes.UPDATE_RECIPE).pipe(
    switchMap(() => this.recipeStoreService.getSelectedRecipe().pipe(take(1))),
    flatMap(recipe => this.recipeApiService.updateRecipe(recipe.id, recipe)),
    filter(Boolean),
    mergeMap(response => [new GetRecipes(), new RouteToRecipes(response.id)])
  );

  @Effect()
  addRecipe$ = this.actions$.ofType(RecipeActionsTypes.ADD_RECIPE).pipe(
    switchMap(() => this.recipeStoreService.getSelectedRecipe().pipe(take(1))),
    flatMap(recipe => this.recipeApiService.createRecipe(recipe.id, recipe)),
    filter(Boolean),
    mergeMap(response => [new GetRecipes(), new RouteToRecipes(response.id)])
  );

  constructor(
    private actions$: Actions,
    private recipeApiService: RecipeApiService,
    private router: Router,
    private recipeStoreService: RecipeStoreService
  ) {}
}
