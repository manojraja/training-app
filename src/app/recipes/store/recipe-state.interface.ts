import { IRecipe } from '../interfaces/recipe.interface';
import { IAppState } from '../../store/app-state.interface';

export interface IRecipeAppState extends IAppState {
  recipes: IRecipeState;
}

export interface IRecipeState {
  recipes: Array<IRecipe>;
  selectedRecipe: IRecipe;
}
