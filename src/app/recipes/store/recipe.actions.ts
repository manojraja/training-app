import { Action } from '@ngrx/store';
import { IRecipe } from '../interfaces/recipe.interface';

export enum RecipeActionsTypes {
  GET_RECIPES = '[Recipe] Get recipes from DB',
  SET_RECIPES = '[Recipe] Set Recipes',
  ADD_RECIPE = '[Recipe] Add Recipe to DB',
  UPDATE_RECIPE = '[Recipe] Update recipe to DB',
  DELETE_RECIPE = '[Recipe] Delete Recipe from DB',
  GO_TO_RECIPES = '[Recipe] Route to recipes',
  SET_SELECTED_RECIPE = '[Recipe] Set selected recipe',
  GET_SELECTED_RECIPE = '[Recipe] Get selected recipe',
  UPDATE_SELECTED_RECIPE = '[Recipe] Update selected recipe',
  ADD_INGREDIENT_TO_SELECTED_RECIPE = '[Recipe] Add ingredient to selected recipe',
  DELETE_INGREDIENT_FROM_SELECTED_RECIPE = '[Recipe] Delete ingredient from selected recipe'
}

export class GetRecipes implements Action {
  readonly type = RecipeActionsTypes.GET_RECIPES;
}

export class SetRecipes implements Action {
  readonly type = RecipeActionsTypes.SET_RECIPES;
  constructor(public recipes: Array<IRecipe>) {}
}

export class AddRecipe implements Action {
  readonly type = RecipeActionsTypes.ADD_RECIPE;
}

export class UpdateRecipe implements Action {
  readonly type = RecipeActionsTypes.UPDATE_RECIPE;
}

export class DeleteRecipe implements Action {
  readonly type = RecipeActionsTypes.DELETE_RECIPE;
  constructor(public index: number) {}
}

export class RouteToRecipes implements Action {
  readonly type = RecipeActionsTypes.GO_TO_RECIPES;
  constructor(public id?: number) {}
}

export class SetSelectedRecipe implements Action {
  readonly type = RecipeActionsTypes.SET_SELECTED_RECIPE;
  constructor(public recipe: IRecipe) {}
}

export class GetSelectedRecipe implements Action {
  readonly type = RecipeActionsTypes.GET_SELECTED_RECIPE;
}

export class UpdateSelectedRecipe implements Action {
  readonly type = RecipeActionsTypes.UPDATE_SELECTED_RECIPE;
  constructor(public key: any, public value: any) {}
}

export class AddIngredientToSelectedRecipe implements Action {
  readonly type = RecipeActionsTypes.ADD_INGREDIENT_TO_SELECTED_RECIPE;
}

export class DeleteIngredientFromSelectedRecipe implements Action {
  readonly type = RecipeActionsTypes.DELETE_INGREDIENT_FROM_SELECTED_RECIPE;
  constructor(public index: number) {}
}

export type RecipeActions =
  | GetRecipes
  | SetRecipes
  | AddRecipe
  | UpdateSelectedRecipe
  | DeleteRecipe
  | SetSelectedRecipe
  | GetSelectedRecipe
  | RouteToRecipes
  | AddIngredientToSelectedRecipe
  | DeleteIngredientFromSelectedRecipe;
