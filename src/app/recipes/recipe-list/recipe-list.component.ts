import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IRecipe } from '../interfaces/recipe.interface';
import { RecipeStoreService } from '../services/recipe-store.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  public recipeStream$: Observable<Array<IRecipe>>;

  constructor(private recipeStoreService: RecipeStoreService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.recipeStream$ = this.recipeStoreService.getRecipes();
  }

  public goToNewRecipe() {
    this.recipeStoreService.createNewRecipe();
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
