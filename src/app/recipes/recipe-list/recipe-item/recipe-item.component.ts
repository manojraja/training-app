import { Component, Input } from '@angular/core';
import { IRecipe } from '../../interfaces/recipe.interface';
import { RecipeStoreService } from '../../services/recipe-store.service';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent {
  @Input()
  recipe: IRecipe;

  constructor(private recipeStoreService: RecipeStoreService) {}

  public setSelectedRecipe() {
    this.recipeStoreService.setSelectedRecipe(this.recipe);
  }
}
