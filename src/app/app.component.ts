import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  public section = 'recipes';
  constructor(private http: HttpClient) {}
  public ngOnInit() {
    this.http
      .get('../assets/firebase.credentials.json')
      .pipe(take(1))
      .subscribe(data => firebase.initializeApp(data));
  }
}
