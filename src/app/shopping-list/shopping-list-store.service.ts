import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { IShoppingListState } from './store/shopping-list-state.interface';
import { Observable } from 'rxjs';
import { Ingredient } from '../shared/ingredient.model';
import {
  AddIngredient,
  UpdateSelectedIngredient,
  UpdateIngredient,
  DeleteIngredient
} from './store/shopping-list.actions';
import { take } from 'rxjs/operators';
import { IAppState } from '../store/app-state.interface';

@Injectable()
export class ShoppingListStoreService {
  constructor(private store: Store<IAppState>) {}

  public getShoppingList(): Observable<IShoppingListState> {
    return this.store.select('shoppingList');
  }

  public getShoppingListIngredients(): Observable<Array<Ingredient>> {
    return this.store.select('shoppingList', 'ingredients');
  }

  public getIngredientByIndex(index): Observable<Ingredient> {
    return this.store.select('shoppingList', 'ingredients', index);
  }

  public getSelectedIngredient(): Observable<Ingredient> {
    return this.store.select('shoppingList', 'selectedIngredient');
  }

  public addIngredient(
    ingredient: Ingredient,
    fromRecipeList: boolean = false
  ) {
    const index = this.getIngredientIndexByName(ingredient.name);
    if (index === -1) {
      this.store.dispatch(new AddIngredient(ingredient));
    } else {
      this.getIngredientByIndex(index)
        .pipe(take(1))
        .subscribe(data => {
          const updatedIngredient = new Ingredient(
            ingredient.name,
            fromRecipeList ? data.quantity + ingredient.quantity : ingredient.quantity
          );
          this.store.dispatch(new UpdateIngredient(updatedIngredient, index));
        });
    }
  }

  public updateSelectedIngredient(ingredient: Ingredient) {
    this.store.dispatch(new UpdateSelectedIngredient(ingredient));
  }

  public deleteIngredient(ingredient: Ingredient) {
    this.store.dispatch(new DeleteIngredient(ingredient));
  }

  public getIngredientIndexByName(ingredientName: string): number {
    let index = -1;
    this.getShoppingListIngredients()
      .pipe(take(1))
      .subscribe(
        ingredients =>
          (index = ingredients.findIndex(
            ingredient =>
              ingredient.name.toLowerCase() === ingredientName.toLowerCase()
          ))
      );
    return index;
  }
}
