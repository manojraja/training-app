import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthRouteGuardService } from '../shared/auth-route-guard.service';
import { ShoppingListComponent } from './shopping-list.component';

const shoppingListRoutes: Routes = [
  {
    path: 'shopping-list',
    canActivate: [AuthRouteGuardService],
    component: ShoppingListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(shoppingListRoutes)],
  exports: [RouterModule]
})
export class ShoppingListRoutingModule {}
