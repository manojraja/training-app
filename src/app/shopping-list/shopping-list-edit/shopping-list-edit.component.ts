import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ShoppingListStoreService } from '../shopping-list-store.service';

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css']
})
export class ShoppingListEditComponent implements OnInit, OnDestroy {
  public unsubscribe = new Subject();

  public form: FormGroup;
  public isEdit = false;

  constructor(private shoppingListStoreService: ShoppingListStoreService) {}

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      quantity: new FormControl(null, [Validators.required])
    });

    this.shoppingListStoreService
      .getSelectedIngredient()
      .pipe(
        filter(Boolean),
        takeUntil(this.unsubscribe)
      )
      .subscribe(selectedIngredient => this.form.setValue(selectedIngredient));

    this.form
      .get('name')
      .valueChanges.pipe(
        takeUntil(this.unsubscribe),
        filter(value => Boolean(value))
      )
      .subscribe(value => {
        this.isEdit =
          this.shoppingListStoreService.getIngredientIndexByName(value) !== -1;
      });
  }

  public addIngredient() {
    this.shoppingListStoreService.addIngredient(this.form.value);
    this.clearForm();
  }

  public deleteIngredient() {
    this.shoppingListStoreService.deleteIngredient(this.form.value);
    this.clearForm();
  }

  public clearForm() {
    this.isEdit = false;
    this.form.reset();
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
