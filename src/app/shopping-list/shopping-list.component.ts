import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { Observable } from 'rxjs';
import { ShoppingListStoreService } from './shopping-list-store.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  public ingredientList: Observable<Array<Ingredient>>;

  constructor(private shoppingListStoreService: ShoppingListStoreService) {}

  public ngOnInit() {
    this.ingredientList = this.shoppingListStoreService.getShoppingListIngredients();
  }

  public updateSelectedIngredient(ingredient: Ingredient) {
    this.shoppingListStoreService.updateSelectedIngredient(ingredient);
  }
}
