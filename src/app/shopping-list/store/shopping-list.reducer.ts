import { Ingredient } from '../../shared/ingredient.model';
import {
  ShoppingListActions,
  ShoppingListActionTypes
} from './shopping-list.actions';
import { IShoppingListState } from './shopping-list-state.interface';

export const initialState: IShoppingListState = {
  ingredients: [new Ingredient('Apples', 5), new Ingredient('Tomatoes', 10)],
  selectedIngredient: null
};

export function shoppingListReducer(
  state = initialState,
  action: ShoppingListActions
): IShoppingListState {
  switch (action.type) {
    case ShoppingListActionTypes.AddIngredient:
      return { ...state, ingredients: [...state.ingredients, action.payload] };
    case ShoppingListActionTypes.UpdateSelectedIngredient:
      return { ...state, selectedIngredient: action.payload };
    case ShoppingListActionTypes.UpdateIngredient:
      return {
        ...state,
        ingredients: [...state.ingredients].map(
          (ingredient, index) =>
            index === action.index ? action.payload : ingredient
        )
      };
    case ShoppingListActionTypes.DeleteIngredient:
      return {
        ...state,
        ingredients: [...state.ingredients].filter(
          ingredient =>
            ingredient.name.toLowerCase() !== action.payload.name.toLowerCase()
        )
      };
    default:
      return state;
  }
}
