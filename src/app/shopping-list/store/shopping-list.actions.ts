import { Action } from '@ngrx/store';
import { Ingredient } from '../../shared/ingredient.model';

export enum ShoppingListActionTypes {
  AddIngredient = '[ShoppingList] Add Ingredient',
  UpdateSelectedIngredient = '[ShoppingList] Update Selected Ingredient',
  UpdateIngredient = '[ShoppingList] Update Ingredient',
  DeleteIngredient = '[ShoppingList] Delete Ingredient'
}

export class AddIngredient implements Action {
  readonly type = ShoppingListActionTypes.AddIngredient;
  constructor(public payload: Ingredient) {}
}

export class UpdateSelectedIngredient implements Action {
  readonly type = ShoppingListActionTypes.UpdateSelectedIngredient;
  constructor(public payload: Ingredient) {}
}

export class UpdateIngredient implements Action {
  readonly type = ShoppingListActionTypes.UpdateIngredient;
  constructor(public payload: Ingredient, public index: number) {}
}

export class DeleteIngredient implements Action {
  readonly type = ShoppingListActionTypes.DeleteIngredient;
  constructor(public payload: Ingredient) {}
}

export type ShoppingListActions =
  | AddIngredient
  | UpdateSelectedIngredient
  | UpdateIngredient
  | DeleteIngredient;
