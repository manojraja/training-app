import { Ingredient } from '../../shared/ingredient.model';

export interface IShoppingListState {
  ingredients: Array<Ingredient>;
  selectedIngredient: Ingredient;
}
