import { Component, OnInit } from '@angular/core';
import { AuthStoreService } from '../../auth/auth-store.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public isUserAuthenticated$: Observable<boolean>;
  constructor(private authStoreService: AuthStoreService) {}

  ngOnInit() {
    this.isUserAuthenticated$ = this.authStoreService.getUserLoggedInFlag();
  }
}
