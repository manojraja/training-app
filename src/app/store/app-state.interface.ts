import { IAuthState } from '../auth/store/auth-state.interface';
import { IShoppingListState } from '../shopping-list/store/shopping-list-state.interface';

export interface IAppState {
  shoppingList: IShoppingListState;
  auth: IAuthState;
}
