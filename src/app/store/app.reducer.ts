import { ActionReducerMap } from '@ngrx/store';
import { IAppState } from './app-state.interface';
import { shoppingListReducer } from '../shopping-list/store/shopping-list.reducer';
import { authReducer } from '../auth/store/auth.reducer';

export const appReducers: ActionReducerMap<IAppState> = {
  shoppingList: shoppingListReducer,
  auth: authReducer
};
